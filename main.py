from eml_adapter import EmlAdapter
from pprint import pprint
from helper import extract_words
import glob
import re

# First of all, sorry for the mess. I hope it is somewhat human readable.
# This whole construct is not optimized, seeing how I could have put the different functionalities
# in their own respective little classes to make things more structured, organized, and less prone to errors, etc.
# Nevertheless, I still hope the functionality is clear.
# Project can be found on GitLab: https://gitlab.com/scamlab/assignment4

usage = input('File(f) or Directory(d)? ')
path = input('Enter path:') 

# If directory
if usage == "d":

	# Get all .eml files in the directory (theoretically also subdirectory, but somehow didn't work for me)
	files = [f for f in glob.glob(path + "**/*.eml", recursive=True)]

	# and recursively execute following code for each file:
	for f in files:
		adapter = EmlAdapter()
		adapter.parse(f)
		body = adapter.email.get('body')[0].get('content')
		subject = adapter.email.get('header').get('subject')
		# Extracted words from email body and subject.
		text = extract_words(body) + " " + extract_words(subject)
		# Get rid of anything that is not a number, a letter, a space, or a few special symbols I deemed relevant.
		text = re.sub('[^A-Za-z0-9$€@ ]+', '', text)
		# "Tokenize" all the words from the email body and subject, aka make a list out of them.
		listi = text.split()
		# Get a list of scamwords that were predefined in the text file (one word per line).
		scamwords = open('scamphrases.txt').read().splitlines()

		# If the count of "scammy" words in the mail divided by overall words exceeds 4 percent and
		# the count of "scammy" words in the mail is equal or bigger than 2, mark as scam.
		if (len([x for x in listi if x in scamwords]))/len(listi)>=0.045 and len([x for x in listi if x in scamwords]) >= 2:
			pprint(f + " is scam!")
			# For each of the four catergories, there another predefined text file with words.
			# Get the count for each of the categories in the mail.
			blackmail = (len([x for x in listi if x in (open('Blackmail.txt').read().splitlines())]))
			giveaway = (len([x for x in listi if x in (open('Giveaway.txt').read().splitlines())]))
			phishing = (len([x for x in listi if x in (open('Phishing.txt').read().splitlines())]))
			empathy = (len([x for x in listi if x in (open('Request_for_Help.txt').read().splitlines())]))
			# Dictionary to better compare.
			var = {blackmail:"Blackmail",giveaway:"Giveaway",phishing:"Phishing",empathy:"Request for Help"}
			# Mail is marked as category with the highest count of its words inside the email.
			pprint("Catergory of Scam: " + var.get(max(var)))

		else:
			# If not categprized as spam, is ham.
			pprint(f + " is ham!")	

# If file, do the same as above described already, but only on the one file.
else:
	adapter = EmlAdapter()
	adapter.parse(path)
	body = adapter.email.get('body')[0].get('content')
	subject = adapter.email.get('header').get('subject')
	text = extract_words(body) + " " + extract_words(subject)
	text = re.sub('[^A-Za-z0-9$€@ ]+', '', text)
	listi = text.split()
	scamwords = open('scamphrases.txt').read().splitlines()

	if (len([x for x in listi if x in scamwords]))/len(listi)>=0.045 and len([x for x in listi if x in scamwords]) >= 2:
		pprint(path + " is scam!")
		blackmail = (len([x for x in listi if x in (open('Blackmail.txt').read().splitlines())]))
		giveaway = (len([x for x in listi if x in (open('Giveaway.txt').read().splitlines())]))
		phishing = (len([x for x in listi if x in (open('Phishing.txt').read().splitlines())]))
		empathy = (len([x for x in listi if x in (open('Request_for_Help.txt').read().splitlines())]))
		var = {blackmail:"Blackmail",giveaway:"Giveaway",phishing:"Phishing",empathy:"Request for Help"}
		pprint("Catergory of Scam: " + var.get(max(var)))

	else:
		pprint(path + " is ham!")	


