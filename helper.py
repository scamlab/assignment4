# Will extract (only) the words from parsed mail to prepare for comparison
# https://pypi.org/project/text2html/
import html2text

def extract_words(raw):
    ht = html2text.HTML2Text()
    ht.ignore_images = ht.ignore_links = True
    return ' '.join(ht.handle(raw).lower().split())