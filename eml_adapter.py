# Parser that will decode .eml files to process them further.
# Credits to Edgar Miadzieles
# https://gitlab.com/edgarmiadzieles/hacs_scamlab_2_task/-/blob/master/eml_adapter.py
# and 
# https://pypi.org/project/eml-parser/
import eml_parser

class EmlAdapter:


    def __init__(self):
        self.ep = eml_parser.EmlParser(include_raw_body=True, include_attachment_data=True)
        self.email = None

    def parse(self, file):
        with open(file, 'rb') as fhdl:
            raw_email = fhdl.read()

        self.email = self.ep.decode_email_bytes(raw_email)
