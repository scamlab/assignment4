Usage: 

    - python3 main.py 

You will be prompted to decide if you want to scan a file or a whole directory. 

    - Type f for file and d for directory.
    
After that, please put in the path of the directory or the file (include filename.eml) in the form of /root/Documents/scammail(/sample.eml in case of a file).

Folder "mails" provided with scam and ham mails that were used for orientation of classification!
